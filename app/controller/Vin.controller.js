sap.ui.define([
    'jquery.sap.global',
    'sap/ui/model/Filter',
    'sap/ui/model/json/JSONModel',
    'lamburus/utils/ResConfigManager',
    'sap/m/MessageToast',
    'sap/ui/core/routing/History',
    'sap/ui/core/mvc/Controller'
], function (jQuery, Filter, JSONModel, ResConfigManager, MessageToast, History, Controller) {
    "use strict";

    var VinController = Controller.extend("lamburus.controller.Vin", {
        mainModel: new JSONModel({"workcenter": "", "vins": [], "vin": ""}),
        resBundle: new ResConfigManager(),
        onInit: function () {

            var params = jQuery.sap.getUriParameters(window.location.href);
            console.log(params);

            var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            oRouter.getRoute("vin").attachPatternMatched(this._onObjectMatched, this);

            this.getView().setModel(this.mainModel);

        },
        _onObjectMatched: function (oEvent) {
            var folderId = oEvent.getParameter("arguments").folderId;
            this.mainModel.setProperty("/workcenter", folderId);
            this.getVins("8800", folderId);
        },
        onAfterRendering: function () {

        },
        onToBackPage: function (event) {

            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();

            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                oRouter.navTo("folder", true);
            }

        },
        onToTmpPage: function (event) {

            this.getOwnerComponent().getRouter().navTo("vin");

        },
        getVins: function (site, folderId) {
            if (!site) {
                site = window.site;
            }
            if (!site) {
                return;
            }

            var that = this;

            var transactionName = "GetVIN";

            var transactionCall = site + "/" + "TRANSACTION" + "/" + transactionName;

            var params = {
                "TRANSACTION": transactionCall,
                "site": site,
                "work_center_bo": folderId,
                "resource_bo": "",
                "OutputParameter": "JSON"
            };

            var authFunc = function (xhr) {};
            //TODO: only for develop in local: remove in test
            var username, password;
            if (jQuery.sap.getUriParameters().get("localMode") === "true") {
                jQuery.ajax({
                    dataType: "json",
                    url: "model/pwd4proxy.json",
                    success: function (data, response) {
                        username = data.usr;
                        password = data.pwd;
                        params.j_user = username;
                        params.j_password = password;
                    },
                    async: false
                });
                authFunc = function (xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(username + ":" + password));
                };
            }

            try {
                var req = jQuery.ajax({
                    url: "/XMII/Runner",
                    data: params,
                    method: "POST",
                    dataType: "xml",
                    async: true
                });
                req.done(jQuery.proxy(that.vinsSuccess, that));
                req.fail(jQuery.proxy(that.vinsError, that));
            } catch (err) {
                jQuery.sap.log.debug(err.stack);
            }

            if ("" === window.site) {
                this.openMessageDialog(that.resourceManager.getResourceBundle().getText("lamburus.noSiteForUser"));
            }
        },
        vinsSuccess: function (data, response) {
            sap.ui.core.BusyIndicator.hide();

            var model = this.getView().getModel();
            var jsonArrStr = jQuery(data).find("Row").text();
            var jsonArrTmp = JSON.parse(jsonArrStr); // jshint ignore:line
            var jsonArr = jsonArrTmp;
            model.setProperty("/vins", jsonArr);
            //model.setProperty("/workcenterStatusSize", jsonArr.length);
        },
        vinsError: function (error) {
            sap.ui.core.BusyIndicator.hide();
        },
        handleValueHelp: function (oEvent) {
            var sInputValue = oEvent.getSource().getValue();

            this.inputLineId = oEvent.getSource().getId();
            // create value help dialog
            if (!this._valueHelpDialog) {
                this._valueHelpDialog = sap.ui.xmlfragment(
                        "lamburus.view.VinDialog",
                        this);
                this.getView().addDependent(this._valueHelpDialog);
            }

            // create a filter for the binding
            var oFilter = new Filter("VIN",
                    sap.ui.model.FilterOperator.Contains, sInputValue
                    );
            this._valueHelpDialog.getBinding("items").filter([oFilter]);

            // open value help dialog filtered by the input value
            this._valueHelpDialog.open(sInputValue);
        },
        _handleValueHelpSearch: function (evt) {
            var sValue = evt.getParameter("value");
            var oFilter = new Filter("VIN",
                    sap.ui.model.FilterOperator.Contains, sValue
                    );
            evt.getSource().getBinding("items").filter([oFilter]);
        },
        _handleValueHelpClose: function (evt) {
            var oSelectedItem = evt.getParameter("selectedItem");
            if (oSelectedItem) {
                var productInput = this.getView().byId(this.inputLineId);
                productInput.setValue(oSelectedItem.getTitle());
            }
            evt.getSource().getBinding("items").filter([]);
        },
        onMessageErrorDialogPress: function (oEvent, obj) {
            
            var folderId = this.mainModel.getProperty("/workcenter");
            var msgLoc = obj.messageError;
            
            var title = this.resBundle.getResourceBundle().getText("lamburus.error.dialog.title");
            var msg = this.resBundle.getResourceBundle().getText("bonflogin.logon." + msgLoc, folderId);
            
            var dialog = new sap.m.Dialog({
                title: title,
                type: 'Message',
                state: 'Error',
                content: new sap.m.Text({
                    text: msg
                }),
                beginButton: new sap.m.Button({
                    text: 'OK',
                    press: function () {
                        dialog.close();
                    }
                }),
                afterClose: function () {
                    dialog.destroy();
                }
            });

            dialog.open();
        }

    });

    return VinController;

});
